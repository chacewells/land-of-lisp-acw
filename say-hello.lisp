; (defun say-hello ()
;     (prin1 "Please type your name:")
;     (let ((name (read)))
;       (print "Nice to meet you, ")
;       (print name)))

; say-hello in a human-readable way
(defun say-hello ()
   (princ "Please type your name:")
   (let ((name (read-line)))
     (princ "Nice to meet you, ")
     (princ name)))
